
public class _01_MyLinkedList_v1 {

    public static void main(String[] args) {
        MyLinkedListV1<String> myList = new MyLinkedListV1<>();
        myList.add("Erstes Element");
        myList.add("Zweites Element");
        myList.add("Drittes Element");

       myList.get(0);
        myList.isEmpty();
        myList.insert("Hallo", 2);
        myList.get(1);

    }
}

//Analog zu LinkedList in Java / Datentyp T für Typ /wenn bei erzeugung in main ein Integer verwindet wird, wird überall Integer verwendet
class MyLinkedListV1<T>{

    public Element<T> head;
    public int count = 0;
    public Element<T> last;

    public void add(T data) {
        Element<T> currentElement = new Element<>(data);

        if (this.last != null) {
            currentElement.index = last.index + 1;
            currentElement.previousElement = last;
            last.nextElement = currentElement;
        }else {
            currentElement.previousElement = null;
            currentElement.index = 0;
        }
        this.last = currentElement;
        this.count++;
    }

    public boolean isEmpty() {
        return this.count == 0;
    }

    public T get(int index){
        //error handling
        if(this.isEmpty() || index < 0 || index > this.count){
            return null;
        }

        Element<T> currentElement;
        int count = last.index - index;
        currentElement = last;

        while (count > 0) {
            currentElement = currentElement.previousElement;
            count--;
        }

        return currentElement.data;
    }

    public Element<T> getElement(int index){
        //error handling
        if(this.isEmpty() || index < 0 || index > this.count){
            return null;
        }

        int count = last.index - index;
        Element<T> currentElement = last;

        while (count > 0){
            currentElement = currentElement.previousElement;
            count--;
        }

        return currentElement;
    }

    public boolean insert(T data, int index){
        Element<T> currentElement;

        if (this.get(index) != null){
            Element<T> newElement = new Element<T>(data);
            currentElement = this.getElement(index);
            newElement.index = index;

            if (currentElement.previousElement != null) {
                currentElement.previousElement.nextElement = newElement;
                newElement.previousElement = currentElement.previousElement;
                currentElement.previousElement = newElement;
                newElement.previousElement = currentElement;
            } else {
                currentElement.previousElement = newElement;
                newElement.nextElement = currentElement;
            }
            currentElement = newElement;

            while (currentElement.nextElement != null){
                currentElement = currentElement.nextElement;
                currentElement.index++;
            }

            this.count++;
            return true;
        } else {
            return false; // error handling
        }
    }

    private Element<T> lastElement() {
        Element<T> lastElement = head;

        while (lastElement.nextElement != null){
            lastElement = lastElement.nextElement;
        }

        return lastElement;
    }

    @Override
    public String toString(){
        String returnString = "";

        Element<T> currentElement = head;

        while (currentElement != null){
            returnString += currentElement.data;
            returnString+= "\n";
            currentElement = currentElement.nextElement;
        }

        return returnString;
    }
}

//<D> wie eine Variable zusehen / D für Daten / nur innerhalb Element
class Element<D>{
    //Datentyp D
    public D data;
    public int index;
    public Element<D> nextElement;
    public Element<D> previousElement;

    public Element(D data) {

        this.data = data;
    }
}
